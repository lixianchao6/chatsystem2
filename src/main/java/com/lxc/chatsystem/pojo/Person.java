package com.lxc.chatsystem.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author lxc 2021/12/13
 */
@Data
public class Person {
    private String username, password, realName;
    private int age, telphone;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;
    private String interesting, sex, headphoto;
    private String imageString;
    private List<Person> friends;

    //constructor
    public Person() {
    }

    public Person(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Person(String username, String password, String realName, int age, int telphone, Date birthday,
                  String interesting, String sex, String headphoto, String imageString) {
        this.username = username;
        this.password = password;
        this.realName = realName;
        this.age = age;
        this.telphone = telphone;
        this.birthday = birthday;
        this.interesting = interesting;
        this.sex = sex;
        this.headphoto = headphoto;
        this.imageString = imageString;
    }

}
