package com.lxc.chatsystem.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author lxc 2021/12/25
 */
@Data
@NoArgsConstructor
@ToString
public class FriendsApply {
    private int id;
    private String fromPerson;
    private String toPerson;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String message;
    private int status;

    public FriendsApply(String fromPerson, String toPerson, Date createTime, String message, int status) {
        this.fromPerson = fromPerson;
        this.toPerson = toPerson;
        this.createTime = createTime;
        this.message = message;
        this.status = status;
    }
}
