package com.lxc.chatsystem.util;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author lxc 2021/12/18
 */
public class HeadImageStringUtils {
    private static String fileLoaction = PropertiesUtils.getProperties("photo.location");

    //根据文件名获取到图片字符串
    public static String getImageString(String fileName){
        if (fileName==null) {
            return null;
        }
        ByteBuffer buffer=null;
        try(FileChannel channel = new RandomAccessFile(new File(fileLoaction,fileName),"r").getChannel()){
             buffer = ByteBuffer.allocate((int) channel.size());
            channel.read(buffer);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (buffer==null) {
            return null;
        }
        //转为读模式
        buffer.flip();
        Charset charset = StandardCharsets.UTF_8;
        CharBuffer charBuffer = charset.decode(buffer);
        return charBuffer.toString();
    }
    //修改头像文件
    public static void modifyImageFile(String fileName,String imageFile){
        File file = new File(fileLoaction, fileName);
        file.delete();
        //图片替换
        try (FileChannel s = new RandomAccessFile(file, "rw").getChannel()) {
            s.write(StandardCharsets.UTF_8.encode(imageFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

