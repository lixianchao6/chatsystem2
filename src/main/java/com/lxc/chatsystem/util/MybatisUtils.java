package com.lxc.chatsystem.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author lxc 2021/12/14
 */
public class MybatisUtils {
    private static SqlSessionFactory factory = null;

    static {
        String config = "mybatis-config.xml";
        InputStream is = null;
        try {
            is = Resources.getResourceAsStream(config);
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            factory = builder.build(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static SqlSession getSqlSession() {
        if (factory!=null) {
            return factory.openSession(true);
        }
        throw new RuntimeException("Mybatis工厂为空");
    }
}
