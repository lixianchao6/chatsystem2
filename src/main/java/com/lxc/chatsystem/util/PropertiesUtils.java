package com.lxc.chatsystem.util;

import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.PropertyPermission;

/**
 * @author lxc 2021/12/14
 */
@Slf4j
public class PropertiesUtils {
    private static Properties properties;

    static {
        properties = new Properties();
//        try {
//            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("serversetting.properties"));
//        } catch (IOException e) {
//            log.debug("配置文件读取错误");
//            e.printStackTrace();
//        }
        try (InputStream is = PropertiesUtils.class.getResourceAsStream("/serversetting.properties")) {
            properties.load(is);
        } catch (IOException e) {
            log.debug("配置文件读取错误");
            e.printStackTrace();
        }
    }
    public static String getProperties(String key) {
        return properties.getProperty(key);
    }
    public static int getServerPort() {
        String value = properties.getProperty("server.port");
        if (value == null) {
            return 8080;
        }
        return Integer.parseInt(value);
    }
}
