package com.lxc.chatsystem.util;

/**
 * @author lxc 2021/12/18
 */

public class UUIDUtils {
    public static String getUUID(){
        return java.util.UUID.randomUUID().toString().replace("-", "");
    }

}