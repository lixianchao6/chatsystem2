package com.lxc.chatsystem;

import com.lxc.chatsystem.protocol.handler.*;
import com.lxc.chatsystem.protocol.codec.MessageCodec;
import com.lxc.chatsystem.util.PropertiesUtils;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lxc 2021/12/13
 */
@SuppressWarnings("all")
@Slf4j
public class Server {
    public static void main(String[] args) {
        int maxFrame = Integer.parseInt(PropertiesUtils.getProperties("max.frame"));
        LoggingHandler loggingHandler = new LoggingHandler();
        MessageCodec messageCodec = new MessageCodec();

        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup(7);

        DataRequestMessageHandler dataRequestMessageHandler = new DataRequestMessageHandler();
        LoginRequestHandler loginRequestHandler = new LoginRequestHandler();
        MyChannelHandlerAdapter myChannelHandlerAdapter = new MyChannelHandlerAdapter();
        ChatRequestMessageHandler chatRequestMessageHandler = new ChatRequestMessageHandler();
        ChangeInformationRequestHandler changeInformationRequestHandler = new ChangeInformationRequestHandler();
        ChatRecordRequestMessageHandler chatRecordRequestMessageHandler = new ChatRecordRequestMessageHandler();
        GetAllFriendsRequestHandler getAllFriendsRequestHandler = new GetAllFriendsRequestHandler();
        RegisterRequestMessageHandler registerRequestMessageHandler = new RegisterRequestMessageHandler();
        FriendsApplyRequestMessageHandler friendsApplyRequestMessageHandler = new FriendsApplyRequestMessageHandler();
        SearchFriendRequestMessageHandler searchFriendRequestMessageHandler = new SearchFriendRequestMessageHandler();
        AddFriendRequestMessageHandler addFriendRequestMessageHandler = new AddFriendRequestMessageHandler();
        AgreeFriendsApplyRequestMessageHandler agreeFriendsApplyRequestMessageHandler = new AgreeFriendsApplyRequestMessageHandler();
        RpcRequestMessageHandler rpcHandler = new RpcRequestMessageHandler();

        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            //option是配置serversocketchannel
            bootstrap.option(ChannelOption.SO_BACKLOG, 1024);
            //给socketchannel配置
            bootstrap.childOption(ChannelOption.SO_KEEPALIVE, true);
            bootstrap.channel(NioServerSocketChannel.class)
                    .group(boss, worker)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            //黏包半包解决方案
                            ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(maxFrame, 9, 4, 3, 0));
                            //日志
                            ch.pipeline().addLast(loggingHandler);
                            //编解码器
                            ch.pipeline().addLast(messageCodec);
                            //连接超时的事件,60s如果没有收到channel的数据，会触发一个IdleState.READER_IDLE事件
                            ch.pipeline().addLast(new IdleStateHandler(600,0,0));
                            //需要用到双向处理器，检测读和写，客户端需要使用在60s内发送心跳包
                            ch.pipeline().addLast(new MyChannelDuplexHandler());
                            //上一步会分别解析消息体和消息头
                            //接收登陆请求的handler
                            ch.pipeline().addLast(loginRequestHandler);
                            //聊天的请求
                            ch.pipeline().addLast(chatRequestMessageHandler);
                            //处理数据请求
                            ch.pipeline().addLast(dataRequestMessageHandler);
                            //通道解绑、异常……
                            ch.pipeline().addLast(myChannelHandlerAdapter);
                            //更新个人信息请求
                            ch.pipeline().addLast(changeInformationRequestHandler);
                            //得到所有的聊天记录
                            ch.pipeline().addLast(chatRecordRequestMessageHandler);
                            //获得所有好友
                            ch.pipeline().addLast(getAllFriendsRequestHandler);
                            //注册
                            ch.pipeline().addLast(registerRequestMessageHandler);
                            //主动获得所有的好友申请
                            ch.pipeline().addLast(friendsApplyRequestMessageHandler);
                            //根据用户名搜索指定好友
                            ch.pipeline().addLast(searchFriendRequestMessageHandler);
                            //添加一个好友的请求
                            ch.pipeline().addLast(addFriendRequestMessageHandler);
                            //同意一个好友的请求
                            ch.pipeline().addLast(agreeFriendsApplyRequestMessageHandler);
                            //Rpc调用请求信息
                            ch.pipeline().addLast(rpcHandler);
                        }
                    });
            ChannelFuture channelFuture = bootstrap.bind(PropertiesUtils.getServerPort()).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }

}