package com.lxc.chatsystem.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 聊天列表的vo
 *
 * @author lxc 2021/12/19
 */
public class ChatListVO {
    private String username;
    private String realName;
    private String message;
    private String comments;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date   createTime;
    private Integer hasRead;
    private String imagePhoto;

    public ChatListVO(String username, String realName, String message, String comments, Date creatTime, Integer hasRead, String imagePhoto) {
        this.username = username;
        this.realName = realName;
        this.message = message;
        this.comments = comments;
        this.createTime = creatTime;
        this.hasRead = hasRead;
        this.imagePhoto = imagePhoto;
    }

    public ChatListVO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getHasRead() {
        return hasRead;
    }

    public void setHasRead(Integer hasRead) {
        this.hasRead = hasRead;
    }

    public String getImagePhoto() {
        return imagePhoto;
    }

    public void setImagePhoto(String imagePhoto) {
        this.imagePhoto = imagePhoto;
    }
}
