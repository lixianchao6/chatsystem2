package com.lxc.chatsystem.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @author lxc 2021/12/25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FriendsApplyVO {
    private int id;
    private String username;
    private String realName;
    private Integer age;
    private String sex;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String message;
    private String headPhoto;
    private String imagePhoto;
}
