package com.lxc.chatsystem.factory;

import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.service.impl.FriendsServiceImpl;

/**
 * @author lxc 2021/12/15
 */
public class FriendsServiceFactory {
    public static FriendsService service =new FriendsServiceImpl();
    public static FriendsService getFriendsService(){
        return service;
    }

    public static Class<?> getImpl(){
        return FriendsServiceImpl.class;
    }
}
