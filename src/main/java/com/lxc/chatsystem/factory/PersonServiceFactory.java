package com.lxc.chatsystem.factory;

import com.lxc.chatsystem.service.PersonService;
import com.lxc.chatsystem.service.impl.PersonServiceImpl;

/**
 * @author lxc 2021/12/15
 */
public class PersonServiceFactory {
    public static PersonService personService=new PersonServiceImpl();
    public static PersonService getPersonService(){
        return personService;
    }
}
