package com.lxc.chatsystem.factory;

import com.lxc.chatsystem.service.ChatRecordService;
import com.lxc.chatsystem.service.impl.ChatRecordServiceImpl;

/**
 * @author lxc 2021/12/20
 */
public class ChatRecordFactory {
    private static ChatRecordService chatRecordService=new ChatRecordServiceImpl();

    public static ChatRecordService getChatRecordService() {
        return chatRecordService;
    }
}
