package com.lxc.chatsystem.factory;

import com.lxc.chatsystem.pojo.FriendsApply;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.service.impl.FriendsApplyServiceImpl;
import com.lxc.chatsystem.service.impl.FriendsServiceImpl;

/**
 * @author lxc 2021/12/15
 */
public class FriendsApplyServiceFactory {
    public static FriendsApplyService service =new FriendsApplyServiceImpl();
    public static FriendsApplyService getFriendsApplyService(){
        return service;
    }
}
