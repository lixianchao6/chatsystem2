package com.lxc.chatsystem.factory;

import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.service.PersonService;
import com.lxc.chatsystem.service.impl.FriendsServiceImpl;
import com.lxc.chatsystem.service.impl.PersonServiceImpl;

/**
 * @author lxc 2021/12/29
 */
public class ServiceFactory {
    public static Class<?> getServiceClass(String serviceName){
        int index = serviceName.lastIndexOf('.');
        String simpleName = serviceName.substring(index + 1);
        switch (simpleName) {
            case "PersonService":
                return PersonServiceImpl.class;
            case "FriendsService":
                return FriendsServiceImpl.class;
            default:
                throw new RuntimeException("没有此类型");
        }
    }
    //根据接口的全限定名
    public static <T> T getService(Class<T> serviceInterface) throws Exception {
        String interfaceName = serviceInterface.getName();
        int index = interfaceName.lastIndexOf('.');
        String simpleName = interfaceName.substring(index + 1);
        //注册中心
        switch (simpleName) {
            case "PersonService":
                return (T) PersonServiceFactory.getPersonService();
            case "FriendsService":
                return (T) FriendsServiceFactory.getFriendsService();
            default:
                System.out.println("接口调用错误");
                throw new Exception("接口调用错误，没有该接口");
        }
    }

}
