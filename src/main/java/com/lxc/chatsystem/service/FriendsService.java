package com.lxc.chatsystem.service;

import com.lxc.chatsystem.vo.FriendsVO;

import java.util.List;

/**
 * @author lxc 2021/12/23
 */
public interface FriendsService {

    /**
     * 改变好友的备注
     * @param personId 本人账号
     * @param friendId 朋友账号
     * @param comments 备注信息
     * @return 返回是否修改成功
     */
    boolean changeComments(String personId,String friendId,String comments);

    List<FriendsVO> getAllFriends(String username);

    void insertFriends(String personId,String friendId);

    FriendsVO getOneFriend(String username,String friendId);
}
