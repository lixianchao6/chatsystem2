package com.lxc.chatsystem.service.impl;

import com.lxc.chatsystem.dao.FriendsApplyMapper;
import com.lxc.chatsystem.dao.PersonMapper;
import com.lxc.chatsystem.pojo.FriendsApply;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.util.HeadImageStringUtils;
import com.lxc.chatsystem.util.MybatisUtils;
import com.lxc.chatsystem.vo.FriendsApplyVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lxc 2021/12/25
 */
@Slf4j
public class FriendsApplyServiceImpl implements FriendsApplyService {
    @Override
    public List<FriendsApply> getAllApply(String username) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsApplyMapper mapper = sqlSession.getMapper(FriendsApplyMapper.class);
        List<FriendsApply> allApply = mapper.getAllApply(username);
        sqlSession.close();
        return allApply;
    }

    @Override
    public List<FriendsApplyVO> getFriendsApplies(String username) {
        List<FriendsApplyVO> result = new ArrayList<>();
        //先取到所有的申请列表
        List<FriendsApply> allApply = getAllApply(username);
        for (FriendsApply apply : allApply) {
            result.add(getOneFriendsApply(apply));
        }
        return result;
    }

    @Override//根据一个好友申请，取到对方的所有信息
    public FriendsApplyVO getOneFriendsApply(FriendsApply apply) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper mapper = sqlSession.getMapper(PersonMapper.class);

        FriendsApplyVO vo = new FriendsApplyVO();
        vo.setId(apply.getId());
        vo.setUsername(apply.getFromPerson());
        vo.setCreateTime(apply.getCreateTime());
        vo.setMessage(apply.getMessage());
        //查找这个人的所有信息
        Person person = mapper.getPersonByUsername(vo.getUsername());
        vo.setAge(person.getAge());
        vo.setRealName(person.getRealName());
        vo.setSex(person.getSex());
        vo.setHeadPhoto(person.getHeadphoto());
        vo.setImagePhoto(HeadImageStringUtils.getImageString(vo.getHeadPhoto()));
        sqlSession.close();
        return vo;
    }

    @Override
    public void insertFriendsApply(FriendsApply friendsApply) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsApplyMapper mapper = sqlSession.getMapper(FriendsApplyMapper.class);
        //插入之前进行一系列的检索
        FriendsApply oneApply = mapper.getOneApply(friendsApply.getFromPerson(), friendsApply.getToPerson());
        //1.不能重复
        if (oneApply==null) {
            mapper.insertFriendsApply(friendsApply);
        }else {
            log.debug("好友请求重复。。。");
        }
        sqlSession.close();
    }

    @Override
    public FriendsApply getOneFriendsApply(int id) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsApplyMapper mapper = sqlSession.getMapper(FriendsApplyMapper.class);
        FriendsApply apply = mapper.getOneApplyById(id);
        sqlSession.close();
        return apply;
    }

    @Override
    public void updateStatus(int id,int status) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsApplyMapper mapper = sqlSession.getMapper(FriendsApplyMapper.class);
        mapper.updateStatus(id,status);
        sqlSession.close();
    }
}
