package com.lxc.chatsystem.service.impl;

import com.lxc.chatsystem.dao.ChatRecordMapper;
import com.lxc.chatsystem.dao.FriendsMapper;
import com.lxc.chatsystem.dao.PersonMapper;
import com.lxc.chatsystem.pojo.ChatRecord;
import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.service.PersonService;
import com.lxc.chatsystem.util.HeadImageStringUtils;
import com.lxc.chatsystem.util.MybatisUtils;
import com.lxc.chatsystem.util.PropertiesUtils;
import com.lxc.chatsystem.util.UUIDUtils;
import com.lxc.chatsystem.vo.ChatListVO;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author lxc 2021/12/14
 */
public class PersonServiceImpl implements PersonService {
    public PersonServiceImpl() {
    }

    @Override
    public Person login(String username, String password) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        Person person = personMapper.getPerson(username, password);
        if(person==null){
            return null;
        }
        person.setImageString(HeadImageStringUtils.getImageString(person.getHeadphoto()));
        sqlSession.close();
        return person;
    }
    @Override//等待更新
    public List<ChatListVO> getChatFriends(String username) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        ChatRecordMapper chatRecordMapper=sqlSession.getMapper(ChatRecordMapper.class);
        FriendsMapper friendsMapper=sqlSession.getMapper(FriendsMapper.class);
        ArrayList<ChatListVO> list = new ArrayList<>();
        //首先查出所有相关的聊天记录
        List<ChatRecord> chatRecords = chatRecordMapper.getChatRecords(username);
        //记录保证不重复
        HashSet<String> friendsUsername = new HashSet<>();
        //从后开始查，因为可以得到最新的消息
        for (int i = chatRecords.size() - 1; i >= 0; i--) {
            ChatRecord record = chatRecords.get(i);
            //去重
            if (friendsUsername.contains(record.getFriendId()) || friendsUsername.contains(record.getPersonId())) {
                continue;
            }
            ChatListVO chatListVO = new ChatListVO();
            //设置固定值
            chatListVO.setCreateTime(record.getCreateTime());
            chatListVO.setMessage(record.getMessage());

            //如果是我发的消息
            if (record.getPersonId().equals(username)) {
                chatListVO.setHasRead(1);
                //把朋友的用户名加入到set里
                friendsUsername.add(record.getFriendId());
                //封装朋友的用户名
                chatListVO.setUsername(record.getFriendId());
                //取到该朋友的照片和真实姓名
                Person friend = personMapper.getPersonByUsername(record.getFriendId());
                chatListVO.setImagePhoto(HeadImageStringUtils.getImageString(friend.getHeadphoto()));
                chatListVO.setRealName(friend.getRealName());
                //查找备注
                Friends friends = friendsMapper.getFriends(record.getPersonId(), record.getFriendId());
                chatListVO.setComments(friends.getComments());
                //加入到集合
                list.add(chatListVO);
            } else {//是别人给我发的消息
                chatListVO.setHasRead(record.getHasRead());
                friendsUsername.add(record.getPersonId());
                //封装朋友的用户名
                chatListVO.setUsername(record.getPersonId());
                //取到该朋友的照片和真实姓名
                Person friend = personMapper.getPersonByUsername(record.getPersonId());
                chatListVO.setImagePhoto(HeadImageStringUtils.getImageString(friend.getHeadphoto()));
                chatListVO.setRealName(friend.getRealName());
                //查找备注
                Friends friends = friendsMapper.getFriends(record.getFriendId(), record.getPersonId());
                chatListVO.setComments(friends.getComments());
                //加入到集合
                list.add(chatListVO);
            }
        }
        sqlSession.close();
        return list;
    }

    //
    @Override
    public Person changeInformation(Person person) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        //取到原始分配的文件名
        String photoName = person.getHeadphoto();
        //修改一下图片内容
        //这是用户修改图片时,如果没有修改图片的话就不用
        if (person.getImageString() != null) {
            HeadImageStringUtils.modifyImageFile(photoName, person.getImageString());
        }
        personMapper.updatePerson(person);
        sqlSession.close();
        return person;
    }

    @Override
    public String insertPerson(Person person) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        Person p = personMapper.getPersonByUsername(person.getUsername());
        if (p!=null){
            return "用户名重复!";
        }
        try {
            //设置默认注册时为放上去的值
            person.setBirthday(new Date());
            person.setSex("男");
            person.setAge(0);
            person.setTelphone(0);
            //插入用户时，定义它的头像文件名
            person.setHeadphoto(UUIDUtils.getUUID() + ".data");
            //把图片放进去
            HeadImageStringUtils.modifyImageFile(person.getHeadphoto(), person.getImageString());
            personMapper.insertPerson(person);
        } catch (Exception e) {
            e.printStackTrace();
            return "插入异常";
        }
        sqlSession.close();
        return "插入成功";
    }

    @Override
    public Person getPersonByUsername(String username) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        Person p = personMapper.getPersonByUsername(username);
        if (p==null){
            return null;
        }
        p.setImageString(HeadImageStringUtils.getImageString(p.getHeadphoto()));
        sqlSession.close();
        return p;
    }
}
