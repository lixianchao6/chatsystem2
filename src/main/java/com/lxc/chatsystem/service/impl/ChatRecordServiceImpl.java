package com.lxc.chatsystem.service.impl;

import com.lxc.chatsystem.dao.ChatRecordMapper;
import com.lxc.chatsystem.pojo.ChatRecord;
import com.lxc.chatsystem.service.ChatRecordService;
import com.lxc.chatsystem.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * @author lxc 2021/12/20
 */
public class ChatRecordServiceImpl implements ChatRecordService {
    @Override
    public List<ChatRecord> getChatMessage(String from, String to) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        ChatRecordMapper mapper= sqlSession.getMapper(ChatRecordMapper.class);
        //1.设置所有的未读消息为已读
        mapper.setOneFriendHasRead(to,from);
        //2.取到对应的聊天记录
        List<ChatRecord> data = mapper.getOneFriendChatRecords(from, to);
        sqlSession.close();
        return data;
    }

    @Override
    public void insertChatRecord(ChatRecord record) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        ChatRecordMapper mapper= sqlSession.getMapper(ChatRecordMapper.class);
        mapper.insertChatRecord(record);
        sqlSession.close();
    }
}
