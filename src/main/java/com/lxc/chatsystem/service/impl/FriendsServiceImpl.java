package com.lxc.chatsystem.service.impl;

import com.lxc.chatsystem.dao.FriendsMapper;
import com.lxc.chatsystem.dao.PersonMapper;
import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.util.HeadImageStringUtils;
import com.lxc.chatsystem.util.MybatisUtils;
import com.lxc.chatsystem.vo.FriendsVO;
import com.sun.imageio.plugins.common.ImageUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lxc 2021/12/23
 */
public class FriendsServiceImpl implements FriendsService {
    @Override
    public boolean changeComments(String personId, String friendId, String comments) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsMapper friendsMapper = sqlSession.getMapper(FriendsMapper.class);
        int i = friendsMapper.updateFriendComments(personId, friendId, comments);
        return i != 0;
    }

    @Override
    public List<FriendsVO> getAllFriends(String username) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsMapper friendsMapper = sqlSession.getMapper(FriendsMapper.class);
        List<FriendsVO> allFriends = friendsMapper.getAllFriends(username);
        for (FriendsVO friendsVO : allFriends) {
            friendsVO.setImagePhoto(HeadImageStringUtils.getImageString(friendsVO.getHeadPhoto()));
        }
        sqlSession.close();
        return allFriends;
    }

    @Override
    public void insertFriends(String personId, String friendId) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsMapper mapper = sqlSession.getMapper(FriendsMapper.class);
        Friends friends = new Friends(personId, friendId, null, new Date());
        Friends f = mapper.getFriends(personId, friendId);
        //插入单向好友
        if (f == null) {//如果为空才插入，否则不插入
            mapper.insertFriends(friends);
        }
        sqlSession.close();
        if (f != null) {
            throw new RuntimeException("已经是好友了");
        }
    }

    @Override
    public FriendsVO getOneFriend(String username, String friendId) {
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        FriendsMapper mapper = sqlSession.getMapper(FriendsMapper.class);
        FriendsVO friends = mapper.getOneFriend(username, friendId);
        friends.setImagePhoto(HeadImageStringUtils.getImageString(friends.getHeadPhoto()));
        sqlSession.close();
        return friends;
    }
}
