package com.lxc.chatsystem.service;

import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.vo.ChatListVO;

import java.util.List;

/**
 * @author lxc 2021/12/14
 */
public interface PersonService {

    Person login(String username, String password);

    List<ChatListVO> getChatFriends(String username);

    Person changeInformation(Person person);

    String insertPerson(Person person);

    Person getPersonByUsername(String username);
}
