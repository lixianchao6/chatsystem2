package com.lxc.chatsystem.service;

import com.lxc.chatsystem.pojo.ChatRecord;

import java.util.List;

/**
 * @author lxc 2021/12/20
 */
public interface ChatRecordService {
    List<ChatRecord> getChatMessage(String from ,String to);

    void insertChatRecord(ChatRecord record);
}
