package com.lxc.chatsystem.service;

import com.lxc.chatsystem.pojo.FriendsApply;
import com.lxc.chatsystem.vo.FriendsApplyVO;

import java.util.List;

/**
 * @author lxc 2021/12/25
 */
public interface FriendsApplyService {

    //获取到别人给我的好友申请
    List<FriendsApply> getAllApply(String username);
    //得到所有的申请好友的详细信息
    List<FriendsApplyVO> getFriendsApplies(String username);
    //得到单条数据
    FriendsApplyVO getOneFriendsApply(FriendsApply friendsApply);
    //增加一条好友申请
    void insertFriendsApply(FriendsApply friendsApply);

    //得到单条数据
    FriendsApply getOneFriendsApply(int id);

    void updateStatus(int id,int status);
}
