package com.lxc.chatsystem;

import com.lxc.chatsystem.protocol.codec.MessageCodec;
import com.lxc.chatsystem.protocol.message.*;
import com.lxc.chatsystem.util.PropertiesUtils;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.concurrent.locks.LockSupport;

/**
 * @author lxc 2021/12/13
 */
@Slf4j
public class NettyClient {
    public static void main(String[] args) {
        String username = "2218", password = "2218";
        NioEventLoopGroup group = new NioEventLoopGroup(3);
        Bootstrap bootstrap = new Bootstrap().group(group).channel(NioSocketChannel.class)

                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) {
                        ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(65535, 9, 4, 3, 0));
                        ch.pipeline().addLast(new LoggingHandler());
                        ch.pipeline().addLast(new MessageCodec());
                        //业务相关的handler。。。
                        ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                            @Override//连接建立后触发登陆请求，发送给服务器，再read服务器给我发送的response信息
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                System.out.println("发送登录请求");
                                LoginRequestMessage loginRequestMessage = new LoginRequestMessage("2218", "2218");
                                ctx.writeAndFlush(loginRequestMessage);
                                super.channelActive(ctx);
                            }
                        });

                        ch.pipeline().addLast("client service", new SimpleChannelInboundHandler<LoginResponseMessage>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext ctx, LoginResponseMessage msg) {
                                System.out.println(msg);
                                if (msg.isSuccess()) {
                                    System.out.println("登录成功");
                                    ctx.writeAndFlush(new DataRequest("2219"));
                                } else {
                                    System.out.println("登陆失败");
                                    //关闭连接
                                    ctx.channel().close();
                                }
                                ctx.fireChannelRead(msg);
                            }
                        });
                        ch.pipeline().addLast(new SimpleChannelInboundHandler<DataResponse>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext ctx, DataResponse msg) throws Exception {
                                System.out.println(msg);
                                ctx.fireChannelRead(msg);
                            }
                        });


                        ch.pipeline().addLast(new SimpleChannelInboundHandler<ChatResponseMessage>() {
                            @Override
                            protected void channelRead0(ChannelHandlerContext ctx, ChatResponseMessage msg) throws Exception {
                                log.debug("我接受到{}发送来的一条消息{}");
                            }
                        });
                    }
                });

        try {
            //连接服务器阻塞
            ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost", 8081)).sync();

            //关闭通道的阻塞
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            log.error("client", e);
        } finally {
            System.out.println("打工人关闭");
            group.shutdownGracefully();
        }
    }
}
