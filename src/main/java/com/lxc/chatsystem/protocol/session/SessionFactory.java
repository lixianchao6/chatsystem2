package com.lxc.chatsystem.protocol.session;

/**
 * @author lxc 2021/12/15
 */
public class SessionFactory {
    private static Session session = new SessionMemoryImpl();

    public static Session getSession() {
        return session;
    }
}
