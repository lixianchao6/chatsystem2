package com.lxc.chatsystem.protocol.session;

import com.lxc.chatsystem.pojo.Person;
import io.netty.channel.Channel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 封装Session的实体类
 * @author lxc 2021/12/15
 */
@Slf4j
public class IoSession {
    //网络连接channel
    private Channel channel;
    //实体类
    private Person person;
    //保存个人信息的attributes
    private Map<String, Object> attributes = new HashMap<>();

    /**
     * 关闭session
     */
    public void close() {
        try {
            if (this.channel == null) {
                return;
            }
            if (channel.isOpen()) {
                channel.close();
            }
        } catch (Exception e) {
        }
    }
    public IoSession(Channel channel, Person person) {
        this.channel = channel;
        this.person = person;
    }
    public Object getAttribute(String key){
        return attributes.get(key);
    }
    public void setAttribute(String key,Object value){
        attributes.put(key,value);
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
