package com.lxc.chatsystem.protocol.session;

import com.lxc.chatsystem.pojo.Person;
import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 注意是 单例模式
 * @author lxc 2021/11/26
 */
@Slf4j
public class SessionMemoryImpl implements Session {
    private final Map<String, IoSession> usernameIoSessionMap = new ConcurrentHashMap<>();
    private final Map<Channel, IoSession> channelIoSessionMap = new ConcurrentHashMap<>();
//    private final Map<Channel, Map<String, Object>> channelAttributesMap = new ConcurrentHashMap<>();

    @Override
    public void bind(Channel channel, Person person) {
        IoSession ioSession = new IoSession(channel,person);
        usernameIoSessionMap.put(person.getUsername(), ioSession);
        channelIoSessionMap.put(channel, ioSession);
        log.debug("{}用户绑定session连接...",person.getRealName());
    }

    @Override
    public void unbind(Channel channel) {
        IoSession ioSession = channelIoSessionMap.remove(channel);
        if (ioSession==null) {
            return;
        }
        usernameIoSessionMap.remove(ioSession.getPerson().getUsername());
        ioSession.close();
        log.info("关闭{}的session连接", ioSession.getPerson().getRealName());
    }
    @Override
    public Channel getChannel(String username) {
        IoSession ioSession = usernameIoSessionMap.get(username);
        if (ioSession==null){
            return null;
        }
        return ioSession.getChannel();
    }
    @Override
    public Object getAttribute(Channel channel, String name) {
        return channelIoSessionMap.get(channel).getAttribute(name);
    }
    @Override
    public void setAttribute(Channel channel, String name, Object value) {
        channelIoSessionMap.get(channel).setAttribute(name,value);
    }
}
