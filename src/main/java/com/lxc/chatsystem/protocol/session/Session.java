package com.lxc.chatsystem.protocol.session;


import com.lxc.chatsystem.pojo.Person;
import io.netty.channel.Channel;

/**
 * @author lxc 2021/11/26
 */
public interface Session {
    /**
     * 绑定会话,添加新会话
     */
    void bind(Channel channel, Person person);
//    void bind(Channel channel, IoSession session);

    //解绑
    void unbind(Channel channel);

    //根据用户名获取channel
    Channel getChannel(String username);


    //获取和设置属性
    Object getAttribute(Channel channel,String name);
    void setAttribute(Channel channel,String name,Object value);

}
