package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.FriendsApplyServiceFactory;
import com.lxc.chatsystem.pojo.FriendsApply;
import com.lxc.chatsystem.protocol.message.AddFriendRequestMessage;
import com.lxc.chatsystem.protocol.message.AddFriendResponseMessage;
import com.lxc.chatsystem.protocol.session.SessionFactory;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.vo.FriendsApplyVO;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lxc 2021/12/27
 */
@Slf4j
@ChannelHandler.Sharable
public class AddFriendRequestMessageHandler extends SimpleChannelInboundHandler<AddFriendRequestMessage> {
    FriendsApplyService service = FriendsApplyServiceFactory.getFriendsApplyService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AddFriendRequestMessage msg) throws Exception {
        FriendsApply data = msg.getData();
        service.insertFriendsApply(data);
        //拿到对方的channel
        Channel toChannel = SessionFactory.getSession().getChannel(data.getToPerson());
        if (toChannel == null) {
            log.debug("对方不在线...");
        } else {
            //对方在线的情况下，我们还需要发送一条消息给对方，达到实时效果
            FriendsApplyVO vo = service.getOneFriendsApply(data);
            toChannel.writeAndFlush(new AddFriendResponseMessage(true,"OK",vo));
        }
    }
}
