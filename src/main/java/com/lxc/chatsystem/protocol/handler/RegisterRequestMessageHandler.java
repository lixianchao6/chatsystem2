package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.PersonServiceFactory;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.protocol.message.RegisterRequestMessage;
import com.lxc.chatsystem.protocol.message.RegisterResponseMessage;
import com.lxc.chatsystem.service.PersonService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lxc 2021/12/24
 */
@ChannelHandler.Sharable
public class RegisterRequestMessageHandler extends SimpleChannelInboundHandler<RegisterRequestMessage> {
    PersonService personService= PersonServiceFactory.getPersonService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RegisterRequestMessage registerRequestMessage) throws Exception {
        System.out.println("注册...");
        Person person = registerRequestMessage.getPerson();
        String s = personService.insertPerson(person);
        RegisterResponseMessage response=null;
        if ("插入成功".equals(s)){
            response=new RegisterResponseMessage(true,s);
        }else{
            response=new RegisterResponseMessage(false,s);
        }
        ctx.writeAndFlush(response);
    }
}
