package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.Server;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lxc 2021/12/29
 */
@Slf4j
public class MyChannelDuplexHandler extends ChannelDuplexHandler {
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        IdleStateEvent event = (IdleStateEvent) evt;
        if (event.state() == IdleState.READER_IDLE) {
            log.debug("60s没有收到客户端的消息了...");
            //关闭掉该连接，避免浪费资源
            ctx.channel().close();
        }
    }
}
