package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.ChatRecordFactory;
import com.lxc.chatsystem.pojo.ChatRecord;
import com.lxc.chatsystem.protocol.message.ChatRequestMessage;
import com.lxc.chatsystem.protocol.message.ChatResponseMessage;
import com.lxc.chatsystem.protocol.session.SessionFactory;
import com.lxc.chatsystem.service.ChatRecordService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * 服务器接收客户端的聊天请求，转发消息
 *
 * @author lxc 2021/12/15
 */
@Slf4j
@ChannelHandler.Sharable
public class ChatRequestMessageHandler extends SimpleChannelInboundHandler<ChatRequestMessage> {
    ChatRecordService service = ChatRecordFactory.getChatRecordService();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ChatRequestMessage msg) throws Exception {
        String from = msg.getFrom();
        String to = msg.getTo();
        //消息保存在数据库里
        ChatRecord record = new ChatRecord(from,to,0,new Date(),msg.getContent());
        //取到对方的通道
        Channel channel = SessionFactory.getSession().getChannel(to);
        //如果对方在线，就拿到了对应的通道
        if (channel != null) {
            log.debug("服务器捕获到:{}用户发送给{}一条消息", from, to);
            log.debug("转发消息给{}",to);
            //立即给对方发送一条收到消息的通知
            channel.writeAndFlush(new ChatResponseMessage(record));
        } else {//如果对方不在线就没有通道，此时我们只需要把消息插入到数据库即可
            log.debug("服务器捕获到:{}用户发送给{}一条消息,但是{}不在线或者是没有此用户", from, to, to);
//            ctx.writeAndFlush(new ChatResponseMessage(false, "对方不在线"));
        }
        service.insertChatRecord(record);
    }
}
