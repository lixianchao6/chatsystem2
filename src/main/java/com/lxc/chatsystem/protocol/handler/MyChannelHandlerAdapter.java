package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.protocol.session.Session;
import com.lxc.chatsystem.protocol.session.SessionFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lxc 2021/12/17
 */
@Slf4j
@ChannelHandler.Sharable
public class MyChannelHandlerAdapter extends ChannelInboundHandlerAdapter {
    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        super.channelUnregistered(ctx);
        log.debug("unregistered");

        Session session = SessionFactory.getSession();
        session.unbind(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
        log.debug("Inactive");
    }
}
