package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.PersonServiceFactory;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.protocol.message.LoginRequestMessage;
import com.lxc.chatsystem.protocol.message.LoginResponseMessage;
import com.lxc.chatsystem.protocol.session.SessionFactory;
import com.lxc.chatsystem.service.PersonService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 服务器接收登录请求的handler
 *
 * @author lxc 2021/12/15
 */
@ChannelHandler.Sharable
public class LoginRequestHandler extends SimpleChannelInboundHandler<LoginRequestMessage> {
    private final PersonService service = PersonServiceFactory.getPersonService();


    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginRequestMessage msg) throws Exception {
        Person res = service.login(msg.getUsername(), msg.getPassword());
        LoginResponseMessage response;
        if (res != null) {
            response = new LoginResponseMessage(res, true, "登陆成功");
            //绑定session会话管理
            SessionFactory.getSession().bind(ctx.channel(), res);
        } else {
            response = new LoginResponseMessage(false, "用户名或密码错误");
        }
        ctx.writeAndFlush(response);
    }
}
