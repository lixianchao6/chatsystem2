package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.ServiceFactory;
import com.lxc.chatsystem.protocol.message.RpcRequestMessage;
import com.lxc.chatsystem.protocol.message.RpcResponseMessage;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.lang.reflect.Method;

/**
 * @author lxc 2021/12/29
 */
@ChannelHandler.Sharable
public class RpcRequestMessageHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage msg) {
        //1.准备好返回值
        RpcResponseMessage res = new RpcResponseMessage(null, null);
        //2.设置相同的序列号
        res.setSequenceId(msg.getSequenceId());
        try {
            //3.根据客户端传过来的接口名称字符串得到服务器的对应接口的Class类型，可以使用spring管理更方便
            Class<?> serviceClass = ServiceFactory.getServiceClass(msg.getInterfaceName());
            //4.反射调用对应的方法
            Method method = serviceClass.getMethod(msg.getMethodName(), msg.getParameterTypes());
            Object returnValue = method.invoke(serviceClass.newInstance(), msg.getParameterValues());
            //5.设置返回值
            System.out.println(returnValue);
            res.setReturnValue(returnValue);
        } catch (Exception e) {
            e.printStackTrace();
            //出异常的时候，要返回异常
            res.setExceptionValue(new RuntimeException("远程调用出错:" + e.getCause().getMessage()));
        }
        //返回响应对象给客户端
        ctx.writeAndFlush(res);
    }
}
