package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.FriendsApplyServiceFactory;
import com.lxc.chatsystem.factory.FriendsServiceFactory;
import com.lxc.chatsystem.pojo.FriendsApply;
import com.lxc.chatsystem.protocol.message.AgreeFriendsApplyRequestMessage;
import com.lxc.chatsystem.protocol.message.AgreeFriendsApplyResponseMessage;
import com.lxc.chatsystem.protocol.session.Session;
import com.lxc.chatsystem.protocol.session.SessionFactory;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.vo.FriendsVO;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lxc 2021/12/28
 */
@ChannelHandler.Sharable
public class AgreeFriendsApplyRequestMessageHandler extends SimpleChannelInboundHandler<AgreeFriendsApplyRequestMessage> {
    private final FriendsApplyService service = FriendsApplyServiceFactory.getFriendsApplyService();
    private final FriendsService friendsService=FriendsServiceFactory.getFriendsService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AgreeFriendsApplyRequestMessage msg) throws Exception {
        int applyId = msg.getApplyId();
        //1.拿到那条好友申请
        FriendsApply apply = service.getOneFriendsApply(applyId);
        //设置为status为1
        if (apply!=null){
            service.updateStatus(applyId,1);
        }
        //2.插入两条好友记录
        try {
            friendsService.insertFriends(apply.getFromPerson(),apply.getToPerson());
            friendsService.insertFriends(apply.getToPerson(),apply.getFromPerson());
        } catch (Exception e) {
            e.printStackTrace();
            //可能会重复添加
            return;
        }
        //3.查出该好友的Friend
        FriendsVO from = friendsService.getOneFriend(apply.getFromPerson(), apply.getToPerson());
        FriendsVO to = friendsService.getOneFriend(apply.getToPerson(),apply.getFromPerson());
        //4.取到channel，看是否在线
        Session session = SessionFactory.getSession();
        Channel channelFrom = session.getChannel(apply.getFromPerson());
        Channel channelTo = session.getChannel(apply.getToPerson());
        //5.通知两个好友，要更新好友列表了
        if (channelFrom!=null){
            channelFrom.writeAndFlush(new AgreeFriendsApplyResponseMessage(true,"有新好友列表了",from));
        }
        if (channelTo!=null){
            channelTo.writeAndFlush(new AgreeFriendsApplyResponseMessage(true,"有新好友列表了",to));
        }
    }
}
