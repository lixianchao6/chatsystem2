package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.PersonServiceFactory;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.protocol.message.SearchFriendRequestMessage;
import com.lxc.chatsystem.protocol.message.SearchFriendResponseMessage;
import com.lxc.chatsystem.service.PersonService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lxc 2021/12/27
 */
@ChannelHandler.Sharable
public class SearchFriendRequestMessageHandler extends SimpleChannelInboundHandler<SearchFriendRequestMessage> {
    PersonService service = PersonServiceFactory.getPersonService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, SearchFriendRequestMessage msg) throws Exception {
        String username = msg.getUsername();
        Person p = service.getPersonByUsername(username);
        if (p == null) {
            ctx.writeAndFlush(new SearchFriendResponseMessage(false, "未查询到任何用户", null));
        } else {
            ctx.writeAndFlush(new SearchFriendResponseMessage(true, "查询成功", p));
        }
    }
}
