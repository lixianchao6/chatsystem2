package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.ChatRecordFactory;
import com.lxc.chatsystem.pojo.ChatRecord;
import com.lxc.chatsystem.protocol.message.MessageRecordRequest;
import com.lxc.chatsystem.protocol.message.MessageRecordResponse;
import com.lxc.chatsystem.service.ChatRecordService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lxc 2021/12/20
 */
@ChannelHandler.Sharable
public class ChatRecordRequestMessageHandler extends SimpleChannelInboundHandler<MessageRecordRequest> {
    private final ChatRecordService chatRecordService= ChatRecordFactory.getChatRecordService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MessageRecordRequest chatRequestMessage) throws Exception {
        try {
            List<ChatRecord> chatRecord = chatRecordService.getChatMessage(chatRequestMessage.getFromUsername(), chatRequestMessage.getToUsername());
            ctx.writeAndFlush(new MessageRecordResponse(chatRecord,true,"success"));
        } catch (Exception e) {
            e.printStackTrace();
            ctx.writeAndFlush(new MessageRecordResponse(false,"请求聊天异常"));
        }

    }
}
