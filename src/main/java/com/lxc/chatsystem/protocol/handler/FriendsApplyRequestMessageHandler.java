package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.FriendsApplyServiceFactory;
import com.lxc.chatsystem.protocol.message.FriendsApplyRequestMessage;
import com.lxc.chatsystem.protocol.message.FriendsApplyResponseMessage;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.vo.FriendsApplyVO;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lxc 2021/12/25
 */
@ChannelHandler.Sharable
public class FriendsApplyRequestMessageHandler extends SimpleChannelInboundHandler<FriendsApplyRequestMessage> {
    FriendsApplyService service = FriendsApplyServiceFactory.getFriendsApplyService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FriendsApplyRequestMessage msg) throws Exception {
        String username = msg.getUsername();
        try {
            List<FriendsApplyVO> result = service.getFriendsApplies(username);
            ctx.writeAndFlush(new FriendsApplyResponseMessage(true,"请求成功",result));
        } catch (Exception e) {
            e.printStackTrace();
            ctx.writeAndFlush(new FriendsApplyResponseMessage(false,"服务器异常",null));
        }
    }
}
