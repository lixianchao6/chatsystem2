package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.FriendsApplyServiceFactory;
import com.lxc.chatsystem.factory.FriendsServiceFactory;
import com.lxc.chatsystem.protocol.message.GetAllFriendsRequest;
import com.lxc.chatsystem.protocol.message.GetAllFriendsResponse;
import com.lxc.chatsystem.service.FriendsApplyService;
import com.lxc.chatsystem.service.FriendsService;
import com.lxc.chatsystem.vo.FriendsVO;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.List;

/**
 * @author lxc 2021/12/23
 */
@ChannelHandler.Sharable
public class GetAllFriendsRequestHandler extends SimpleChannelInboundHandler<GetAllFriendsRequest> {
    private final FriendsService service= FriendsServiceFactory.getFriendsService();
    private final FriendsApplyService friendsApplyService= FriendsApplyServiceFactory.getFriendsApplyService();
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GetAllFriendsRequest request) throws Exception {
        try {
            List<FriendsVO> allFriends = service.getAllFriends(request.getUsername());
            ctx.writeAndFlush(new GetAllFriendsResponse(allFriends,
                    friendsApplyService.getAllApply(request.getUsername()).size(),true,"好友请求成功"));
        } catch (Exception e) {
            e.printStackTrace();
            ctx.writeAndFlush(new GetAllFriendsResponse(null,0,false,"请求好友异常"));
        }
    }
}
