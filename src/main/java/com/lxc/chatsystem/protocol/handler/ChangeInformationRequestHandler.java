package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.PersonServiceFactory;
import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.protocol.message.ChangeInformationRequest;
import com.lxc.chatsystem.protocol.message.ChangeInformationResponse;
import com.lxc.chatsystem.service.PersonService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @author lxc 2021/12/18
 */
@ChannelHandler.Sharable
public class ChangeInformationRequestHandler extends SimpleChannelInboundHandler<ChangeInformationRequest> {
    private final PersonService service= PersonServiceFactory.getPersonService();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ChangeInformationRequest msg) throws Exception {
        try {
            Person personAfterModify = service.changeInformation(msg.getPerson());
            ctx.writeAndFlush(new ChangeInformationResponse(personAfterModify, true, "修改成功"));
        } catch (Exception e) {
            e.printStackTrace();
            ctx.writeAndFlush(new ChangeInformationResponse(false, "修改异常"));
        }
    }
}
