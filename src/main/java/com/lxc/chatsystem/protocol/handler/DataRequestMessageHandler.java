package com.lxc.chatsystem.protocol.handler;

import com.lxc.chatsystem.factory.PersonServiceFactory;
import com.lxc.chatsystem.protocol.message.DataRequest;
import com.lxc.chatsystem.protocol.message.DataResponse;
import com.lxc.chatsystem.service.PersonService;
import com.lxc.chatsystem.vo.ChatListVO;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author lxc 2021/12/16
 */
@ChannelHandler.Sharable
@Slf4j
public class DataRequestMessageHandler extends SimpleChannelInboundHandler<DataRequest> {
    private final PersonService service= PersonServiceFactory.getPersonService();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DataRequest dataRequest) throws Exception {
       log.debug("{}向我请求数据",dataRequest.getUsername());
        List<ChatListVO> chatFriends = service.getChatFriends(dataRequest.getUsername());
        ctx.writeAndFlush(new DataResponse(chatFriends));
    }
}
