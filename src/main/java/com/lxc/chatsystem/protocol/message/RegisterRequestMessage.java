package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/24
 */
@Data
public class RegisterRequestMessage extends MyMessage{
    private Person person;

    public RegisterRequestMessage(Person person) {
        this.person = person;
    }

    public RegisterRequestMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.RegisterRequestMessage;
    }
}
