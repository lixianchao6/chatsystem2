package com.lxc.chatsystem.protocol.message;


import lombok.Data;

import java.util.Set;
@Data
public class GroupCreateRequestMessage extends MyMessage {
    private String groupName;
    private Set<String> members;

    public GroupCreateRequestMessage(String groupName, Set<String> members) {
        this.messageType=getMessageType();
        this.groupName = groupName;
        this.members = members;
    }

    @Override
    public int getMessageType() {
        return MessageType.CreateGroupRequestMessage;
    }
}
