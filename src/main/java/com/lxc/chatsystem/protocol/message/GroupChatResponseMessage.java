package com.lxc.chatsystem.protocol.message;

import lombok.Data;

@Data
public class GroupChatResponseMessage extends AbstractResponseMessage {
    private String from;
    private String content;

    public GroupChatResponseMessage(boolean success, String reason) {
        super(success, reason);
        this.messageType=getMessageType();
    }

    public GroupChatResponseMessage(String from, String content) {
        this.messageType=getMessageType();
        this.from = from;
        this.content = content;
    }
    @Override
    public int getMessageType() {
        return MessageType.GroupChatResponseMessage;
    }
}
