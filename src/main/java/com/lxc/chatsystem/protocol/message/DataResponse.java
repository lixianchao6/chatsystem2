package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.vo.ChatListVO;
import lombok.Data;

import java.util.List;

/**
 * @author lxc 2021/12/16
 */
@Data
public class DataResponse extends MyMessage{
    //需要返回的数据
    List<ChatListVO> data;
    @Override
    public int getMessageType() {
        return MessageType.DataResponse;
    }

    public DataResponse() {
        this.messageType=getMessageType();
    }

    public DataResponse(List<ChatListVO> data) {
        this.messageType=getMessageType();
        this.data = data;
    }

    public List<ChatListVO> getData() {
        return data;
    }

    public void setData(List<ChatListVO> data) {
        this.data = data;
    }

}
