package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/27
 */
@Data
public class SearchFriendRequestMessage extends MyMessage{
    private String username;

    public SearchFriendRequestMessage() {
        this.messageType=getMessageType();
    }

    public SearchFriendRequestMessage(String username) {
        this.messageType=getMessageType();
        this.username = username;
    }
    @Override
    public int getMessageType() {
        return MessageType.SearchFriendRequestMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
