package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/28
 */
@Data
public class AgreeFriendsApplyRequestMessage extends MyMessage{
    //只需要获取到对应的好友申请的那条记录即可
    private int applyId;

    public AgreeFriendsApplyRequestMessage(int applyId) {
        this.messageType=getMessageType();
        this.applyId = applyId;
    }

    public AgreeFriendsApplyRequestMessage() {
        this.messageType=getMessageType();

    }

    @Override
    public int getMessageType() {
        return MessageType.AgreeFriendsApplyRequestMessage;
    }

    public int getApplyId() {
        return applyId;
    }

    public void setApplyId(int applyId) {
        this.applyId = applyId;
    }
}
