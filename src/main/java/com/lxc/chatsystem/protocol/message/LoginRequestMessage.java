package com.lxc.chatsystem.protocol.message;

import lombok.Data;

@Data
public class LoginRequestMessage extends MyMessage {
    //登陆请求只需要附带一个账号和密码
    private String username, password, realName;

    @Override
    public int getMessageType() {
        return MessageType.LoginRequestMessage;
    }


    public LoginRequestMessage(String username, String password) {
        this();
        this.username = username;
        this.password = password;
    }

    public LoginRequestMessage(String username, String password, String realName) {
        this();
        this.username = username;
        this.password = password;
        this.realName = realName;
    }

    public LoginRequestMessage() {
        this.messageType=getMessageType();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
