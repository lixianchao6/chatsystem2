package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.FriendsApply;
import lombok.Data;

/**
 * @author lxc 2021/12/27
 */
@Data
public class AddFriendRequestMessage extends MyMessage{
    private FriendsApply data;
    @Override
    public int getMessageType() {
        return MessageType.AddFriendRequestMessage;
    }

    public AddFriendRequestMessage() {
        this.messageType=getMessageType();
    }

    public FriendsApply getData() {
        return data;
    }
    public void setData(FriendsApply data) {
        this.data = data;
    }

    public AddFriendRequestMessage(FriendsApply data) {
        this.data = data;
    }
}
