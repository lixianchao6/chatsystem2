package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/18
 */
@Data
public class ChangeInformationRequest extends MyMessage{
    private Person person;
    public ChangeInformationRequest() {
        messageType=getMessageType();
    }
    public ChangeInformationRequest(Person person) {
        this();
        this.person = person;
    }

    @Override
    public int getMessageType() {
        return MessageType.ChangeInformationRequest;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
