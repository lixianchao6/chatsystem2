package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/18
 */
@Data
public class ChangeInformationResponse extends AbstractResponseMessage{
    private Person person;

    public ChangeInformationResponse(Person person,boolean success, String reason) {
        super(success, reason);
        messageType=getMessageType();
        this.person=person;
    }
    public ChangeInformationResponse(boolean success, String reason) {
        super(success, reason);
        messageType=getMessageType();
    }

    public ChangeInformationResponse() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.ChangeInformationResponse;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
