package com.lxc.chatsystem.protocol.message;


/**
 * @author lxc 2021/12/13
 */
//所有可能的指令类型
public class MessageType {
    public static final int LoginRequestMessage = 0;
    public static final int LoginResponseMessage = 1;

    public static final int RegisterRequestMessage = 2;
    public static final int RegisterResponseMessage = 3;

    public static final int ChatRequestMessage = 4;
    public static final int ChatResponseMessage = 5;

    public static final int GroupChatRequestMessage = 6;
    public static final int GroupChatResponseMessage = 7;

    public static final int AddFriendRequestMessage = 8;
    public static final int AddFriendResponseMessage = 9;

    public static final int CreateGroupRequestMessage = 10;
    public static final int CreateGroupResponseMessage = 11;

    public static final int GroupJoinRequestMessage = 12;
    public static final int GroupJoinResponseMessage = 13;

    public static final int GroupQuitRequestMessage = 14;
    public static final int GroupQuitResponseMessage = 15;

    public static final int DataRequest = 16;
    public static final int DataResponse = 17;

    public static final int ChangeInformationRequest = 18;
    public static final int ChangeInformationResponse = 19;

    public static final int MessageRecordRequest = 20;
    public static final int MessageRecordResponse = 21;

    public static final int GetAllFriendsRequest = 22;
    public static final int GetAllFriendsResponse = 23;

    public static final int FriendApplyRequestMessage = 24;
    public static final int FriendApplyResponseMessage = 25;

    public static final int SearchFriendRequestMessage = 26;
    public static final int SearchFriendResponseMessage = 27;

    public static final int AgreeFriendsApplyRequestMessage = 28;
    public static final int AgreeFriendsApplyResponseMessage = 29;

    public static final int PingMessage=30;
    public static final int PongMessage=31;



    public static final int RpcRequestMessage=100;
    public static final int RpcResponseMessage=101;

    public static Class<? extends MyMessage> getMessageClass(int type) {
        switch (type) {
            case LoginRequestMessage:
                return LoginRequestMessage.class;
            case LoginResponseMessage:
                return LoginResponseMessage.class;
            case ChatRequestMessage:
                return ChatRequestMessage.class;
            case ChatResponseMessage:
                return ChatResponseMessage.class;
            case DataRequest:
                return DataRequest.class;
            case DataResponse:
                return DataResponse.class;
            case ChangeInformationRequest:
                return ChangeInformationRequest.class;
            case ChangeInformationResponse:
                return ChangeInformationResponse.class;
            case MessageRecordRequest:
                return MessageRecordRequest.class;
            case MessageRecordResponse:
                return MessageRecordResponse.class;
            case GetAllFriendsRequest:
                return GetAllFriendsRequest.class;
            case GetAllFriendsResponse:
                return GetAllFriendsResponse.class;
            case RegisterRequestMessage:
                return RegisterRequestMessage.class;
            case RegisterResponseMessage:
                return RegisterResponseMessage.class;
            case FriendApplyRequestMessage:
                return FriendsApplyRequestMessage.class;
            case FriendApplyResponseMessage:
                return FriendsApplyResponseMessage.class;
            case SearchFriendRequestMessage:
                return SearchFriendRequestMessage.class;
            case SearchFriendResponseMessage:
                return SearchFriendResponseMessage.class;
            case AddFriendRequestMessage:
                return AddFriendRequestMessage.class;
            case AddFriendResponseMessage:
                return AddFriendResponseMessage.class;
            case AgreeFriendsApplyRequestMessage:
                return AgreeFriendsApplyRequestMessage.class;
            case AgreeFriendsApplyResponseMessage:
                return AgreeFriendsApplyResponseMessage.class;
            case PingMessage:
                return PingMessage.class;
            case PongMessage:
                return PongMessage.class;
            case RpcRequestMessage:
                return RpcRequestMessage.class;
            case RpcResponseMessage:
                return RpcResponseMessage.class;
            default:
                System.out.println("MessageType没取到...");
                return null;
        }
    }
}