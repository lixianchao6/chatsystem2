package com.lxc.chatsystem.protocol.message;

import lombok.Data;

import java.util.Arrays;

/**
 * @author lxc 2021/12/29
 */
@Data
public class RpcRequestMessage extends MyMessage{
    /**
     * 调用的接口全限定名
     */
    private String interfaceName;
    /**
     * 需要调用的方法名
     */
    private String methodName;
    /**
     * 方法返回值类型
     */
    private Class<?> returnType;
    /**
     * 方法各参数的类型数组
     */
    private Class<?>[] parameterTypes;
    /**
     * 方法的参数值数组
     */
    private Object[] parameterValues;

    public RpcRequestMessage(String interfaceName, String methodName, Class<?> returnType, Class<?>[] parameterTypes, Object[] parameterValues) {
        this.messageType=getMessageType();
        this.interfaceName = interfaceName;
        this.methodName = methodName;
        this.returnType = returnType;
        this.parameterTypes = parameterTypes;
        this.parameterValues = parameterValues;
    }

    public RpcRequestMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.RpcRequestMessage;
    }

    @Override
    public String toString() {
        return "RpcRequestMessage{" +
                "interfaceName='" + interfaceName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", returnType=" + returnType +
                ", parameterTypes=" + Arrays.toString(parameterTypes) +
                ", parameterValues=" + Arrays.toString(parameterValues) +
                '}';
    }
}
