package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/13
 */
@Data
public class LoginResponseMessage extends AbstractResponseMessage {
    private Person person;
    public LoginResponseMessage(Person person, boolean success, String reason) {
        super(success, reason);
        this.messageType=getMessageType();
        this.person=person;
        messageType=MessageType.LoginResponseMessage;
    }

    public LoginResponseMessage() {
        this.messageType=getMessageType();
    }

    public LoginResponseMessage(boolean success, String reason) {
        super(success, reason);
    }

    @Override
    public int getMessageType() {
        return MessageType.LoginResponseMessage;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

}
