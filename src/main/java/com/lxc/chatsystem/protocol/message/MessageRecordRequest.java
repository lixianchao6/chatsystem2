package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/20
 */
@Data
public class MessageRecordRequest extends MyMessage {
    private String fromUsername, toUsername;

    public MessageRecordRequest(String fromUsername, String toUsername) {
        messageType = getMessageType();
        this.fromUsername = fromUsername;
        this.toUsername = toUsername;
    }

    public MessageRecordRequest() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.MessageRecordRequest;
    }

    public String getFromUsername() {
        return fromUsername;
    }

    public void setFromUsername(String fromUsername) {
        this.fromUsername = fromUsername;
    }

    public String getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }
}
