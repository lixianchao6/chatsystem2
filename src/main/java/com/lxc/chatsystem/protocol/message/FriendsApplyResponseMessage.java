package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.vo.FriendsApplyVO;

import java.util.List;

import lombok.Data;

/**
 * @author lxc 2021/12/25
 */
@Data
public class FriendsApplyResponseMessage extends AbstractResponseMessage{
    private List<FriendsApplyVO> data;

    public FriendsApplyResponseMessage() {
        this.messageType=getMessageType();
    }

    public FriendsApplyResponseMessage(boolean success, String reason, List<FriendsApplyVO> data) {
        super(success, reason);
        messageType=getMessageType();
        this.data = data;
    }
    @Override
    public int getMessageType() {
        return MessageType.FriendApplyResponseMessage;
    }
}
