package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import com.lxc.chatsystem.vo.FriendsApplyVO;
import lombok.Data;

/**
 * @author lxc 2021/12/27
 */
@Data
public class AddFriendResponseMessage extends AbstractResponseMessage {
    private FriendsApplyVO data;
    @Override
    public int getMessageType() {
        return MessageType.AddFriendResponseMessage;
    }
    public AddFriendResponseMessage(boolean success, String reason, FriendsApplyVO data) {
        super(success, reason);
        this.data = data;
    }

    public AddFriendResponseMessage() {
        this.messageType=getMessageType();
    }

    public FriendsApplyVO getData() {
        return data;
    }

    public void setData(FriendsApplyVO data) {
        this.data = data;
    }
}
