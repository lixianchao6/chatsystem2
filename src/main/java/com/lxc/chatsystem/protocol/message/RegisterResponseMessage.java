package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/24
 */
@Data
public class RegisterResponseMessage extends AbstractResponseMessage{
    public RegisterResponseMessage(boolean success, String reason) {
        super(success, reason);
        messageType=getMessageType();
    }

    public RegisterResponseMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.RegisterResponseMessage;
    }


}
