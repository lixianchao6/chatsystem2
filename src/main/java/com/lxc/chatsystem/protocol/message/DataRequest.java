package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/16
 */
@Data
public class DataRequest extends MyMessage{
    //请求数据的的用户名
    String username;

    public DataRequest() {
        this.messageType=getMessageType();

    }

    @Override
    public int getMessageType() {
        return MessageType.DataRequest;
    }

    public DataRequest(String username) {
        this.messageType=getMessageType();
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
