package com.lxc.chatsystem.protocol.message;

public class PingMessage extends MyMessage{
    public PingMessage() {
        this.messageType=getMessageType();
    }
    @Override
    public int getMessageType() {
        return MessageType.PingMessage;
    }
}
