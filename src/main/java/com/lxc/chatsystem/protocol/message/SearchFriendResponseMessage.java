package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.Person;
import lombok.Data;

/**
 * @author lxc 2021/12/27
 */
@Data
public class SearchFriendResponseMessage extends AbstractResponseMessage{
    private Person person;

    public SearchFriendResponseMessage() {
        this.messageType=getMessageType();
    }

    public SearchFriendResponseMessage(boolean success, String reason, Person person) {
        super(success, reason);
        this.messageType=getMessageType();
        this.person = person;
    }

    @Override
    public int getMessageType() {
        return MessageType.SearchFriendResponseMessage;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
