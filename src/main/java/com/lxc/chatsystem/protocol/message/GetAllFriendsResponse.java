package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.vo.FriendsVO;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class GetAllFriendsResponse extends AbstractResponseMessage{
    private List<FriendsVO> data;
    private int friendApply;
    public GetAllFriendsResponse(List<FriendsVO> data,boolean success,String s) {
        super(success,s);
        this.data = data;
    }

    public GetAllFriendsResponse(List<FriendsVO> data, int friendApply,boolean success, String reason) {
        super(success, reason);
        this.data = data;
        this.friendApply = friendApply;
    }

    public GetAllFriendsResponse() {
        this.messageType=getMessageType();
    }


    @Override
    public int getMessageType() {
        return MessageType.GetAllFriendsResponse;
    }

    public List<FriendsVO> getData() {
        return data;
    }

    public void setData(List<FriendsVO> data) {
        this.data = data;
    }

    public int getFriendApply() {
        return friendApply;
    }
    public void setFriendApply(int friendApply) {
        this.friendApply = friendApply;
    }
}
