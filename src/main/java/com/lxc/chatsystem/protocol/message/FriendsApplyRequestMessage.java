package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/25
 */
@Data
public class FriendsApplyRequestMessage extends MyMessage{
    private String username;

    public FriendsApplyRequestMessage(String username) {
        this.username = username;
        messageType=getMessageType();
    }

    public FriendsApplyRequestMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.FriendApplyRequestMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
