package com.lxc.chatsystem.protocol.message;

import lombok.Data;

@Data
public class GetAllFriendsRequest extends MyMessage{
    //根据用户名取到所有的好友信息
    private String username;

    public GetAllFriendsRequest() {
        this.messageType=getMessageType();
    }

    public GetAllFriendsRequest(String username) {
        this.messageType=getMessageType();
        this.username = username;
    }

    @Override
    public int getMessageType() {
        return MessageType.GetAllFriendsRequest;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
