package com.lxc.chatsystem.protocol.message;

import lombok.Data;

@Data
public class GroupChatRequestMessage extends MyMessage {
    private String content;
    private String groupName;
    private String from;

    public GroupChatRequestMessage() {
        this.messageType=getMessageType();
    }

    public GroupChatRequestMessage(String from, String groupName, String content) {
        this.messageType=getMessageType();
        this.content = content;
        this.groupName = groupName;
        this.from = from;
    }

    @Override
    public int getMessageType() {
        return MessageType.GroupChatRequestMessage;
    }
}
