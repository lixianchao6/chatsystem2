package com.lxc.chatsystem.protocol.message;

public class PongMessage extends AbstractResponseMessage{
    public PongMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.PongMessage;
    }
}
