package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.pojo.ChatRecord;
import lombok.Data;

import java.util.List;

/**
 * @author lxc 2021/12/20
 */
@Data
public class MessageRecordResponse extends AbstractResponseMessage {
    List<ChatRecord> record;

    public MessageRecordResponse(List<ChatRecord> record,boolean success, String reason) {
        super(success, reason);
        messageType=getMessageType();
        this.record=record;
    }

    public MessageRecordResponse() {
        this.messageType=getMessageType();
    }

    public MessageRecordResponse(boolean success, String reason) {
        super(success, reason);
        messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.MessageRecordResponse;
    }

    public List<ChatRecord> getRecord() {
        return record;
    }

    public void setRecord(List<ChatRecord> record) {
        this.record = record;
    }


}
