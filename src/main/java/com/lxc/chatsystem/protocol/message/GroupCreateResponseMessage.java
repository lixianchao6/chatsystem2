package com.lxc.chatsystem.protocol.message;

import lombok.Data;

@Data
public class GroupCreateResponseMessage extends AbstractResponseMessage {

    public GroupCreateResponseMessage(boolean success, String reason) {
        super(success, reason);
        this.messageType=getMessageType();

    }

    @Override
    public int getMessageType() {
        return MessageType.CreateGroupResponseMessage;
    }
}
