package com.lxc.chatsystem.protocol.message;

import lombok.Data;

/**
 * @author lxc 2021/12/29
 */
@Data
public class RpcResponseMessage extends AbstractResponseMessage{
    /**
     * Rpc调用返回值
     */
    private Object returnValue;
    /**
     * Rpc调用异常信息
     */
    private Exception exceptionValue;

    public RpcResponseMessage(Object returnValue, Exception exceptionValue) {
        this.messageType=getMessageType();
        this.returnValue = returnValue;
        this.exceptionValue = exceptionValue;
    }

    public RpcResponseMessage() {
        this.messageType=getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.RpcResponseMessage;
    }
}
