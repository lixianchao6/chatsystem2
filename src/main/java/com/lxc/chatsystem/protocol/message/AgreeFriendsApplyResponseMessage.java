package com.lxc.chatsystem.protocol.message;

import com.lxc.chatsystem.vo.FriendsVO;
import lombok.Data;

/**
 * @author lxc 2021/12/28
 */
@Data
public class AgreeFriendsApplyResponseMessage extends AbstractResponseMessage {
    //添加成功后，要获得该好友的所有信息,也就是FriendsVO
    private FriendsVO friendsVO;

    public AgreeFriendsApplyResponseMessage(boolean success, String reason, FriendsVO friendsVO) {
        super(success, reason);
        this.messageType = getMessageType();
        this.friendsVO = friendsVO;
    }

    public AgreeFriendsApplyResponseMessage() {
        this.messageType = getMessageType();
    }

    @Override
    public int getMessageType() {
        return MessageType.AgreeFriendsApplyResponseMessage;
    }

    public FriendsVO getFriendsVO() {
        return friendsVO;
    }

    public void setFriendsVO(FriendsVO friendsVO) {
        this.friendsVO = friendsVO;
    }
}
