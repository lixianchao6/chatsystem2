package com.lxc.chatsystem.protocol.group;

import java.nio.channels.Channel;
import java.util.List;
import java.util.Set;

/**
 * @author lxc 2021/11/26
 */
public interface GroupSession {
    //创建一个聊天组
    Group createGroup(String name, Set<String> members);

    //加入成员
    Group joinMember(String name, String member);

    //移除组成员
    Group removeMember(String name, String member);

    //获取组成员
    Set<String> getMembers(String name);

    //获取组成员的channel集合，只有在线的才能显示
    List<Channel> getMembersChannel(String name);
}
