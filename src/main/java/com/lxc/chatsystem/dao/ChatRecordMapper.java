package com.lxc.chatsystem.dao;

import com.lxc.chatsystem.pojo.ChatRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lxc 2021/12/19
 */
public interface ChatRecordMapper {
    //获得所有相关的聊天记录
    List<ChatRecord> getChatRecords(String username);

    List<ChatRecord> getOneFriendChatRecords(@Param("from") String from, @Param("to") String to);

    void setOneFriendHasRead(@Param("from") String from, @Param("to") String to);

    void insertChatRecord(ChatRecord record);
}
