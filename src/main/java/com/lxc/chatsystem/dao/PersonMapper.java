package com.lxc.chatsystem.dao;

import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.pojo.Person;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lxc 2021/12/14
 */
//Person表的dao层
public interface PersonMapper {
    //查询所有数据
    List<Person> getAllPersons();
    //根据用户名查询对应的person
    Person getPersonByUsername(String username);

    //插入数据
    int insertPerson(Person person);
    //登录查询
    Person getPerson(@Param("username") String username, @Param("password") String password);
    //根据用户名查询和他聊天的所有用户
    List<String> getChatFriendsId(String username);

    List<Friends> getChatFriends(@Param("list") List<String> list, @Param("username")String username);

    int updatePerson(Person person);
    //根据用户名获取对应图片
    String getHeadPhoto(String userName);
}
