package com.lxc.chatsystem.dao;

import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.vo.FriendsVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lxc 2021/12/19
 */
public interface FriendsMapper {
    int updateFriendComments(@Param("personId") String personId,@Param("friendId")String friendId,@Param("comments")String comments);

    Friends getFriends(@Param("personId")String username,@Param("friendsId")String friendId);

    List<FriendsVO> getAllFriends(String username);

    void insertFriends(Friends friends);

    FriendsVO getOneFriend(@Param("personId")String username,@Param("friendsId")String friendId);
}
