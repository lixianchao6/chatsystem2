package com.lxc.chatsystem.dao;

import com.lxc.chatsystem.pojo.FriendsApply;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lxc 2021/12/25
 */
public interface FriendsApplyMapper {
    //获取到别人给我的好友申请
    List<FriendsApply> getAllApply(String username);

    void insertFriendsApply(FriendsApply friendsApply);

    FriendsApply getOneApply(@Param("fromId")String fromId,@Param("toId")String toId);

    FriendsApply getOneApplyById(int id);

    void updateStatus(@Param("id")int id, @Param("status")int status);
}
