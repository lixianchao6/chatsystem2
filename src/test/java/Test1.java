import com.lxc.chatsystem.dao.FriendsMapper;
import com.lxc.chatsystem.pojo.Friends;
import com.lxc.chatsystem.util.MybatisUtils;
import com.lxc.chatsystem.vo.FriendsVO;
import org.apache.ibatis.session.SqlSession;


/**
 * @author lxc 2021/12/13
 */
public class Test1 {
    public static void main(String[] args) {
//        PersonService personService = PersonServiceFactory.getPersonService();
//        List<ChatListVO> chatFriends = personService.getChatFriends("2218");
//        for (ChatListVO chatFriend : chatFriends) {
//            System.out.println(chatFriend);
//        }
//        SqlSession sqlSession = MybatisUtils.getSqlSession();
//        FriendsMapper friendsMapper=sqlSession.getMapper(FriendsMapper.class);
//        FriendsVO admin = friendsMapper.getOneFriend("2218","root");
//        System.out.println(admin);
        String interfaceName = "com.java.lxc.charsystem.HelloService";
        int index = interfaceName.lastIndexOf('.');
        String substring = interfaceName.substring(index + 1);
        System.out.println(substring);
    }
}
